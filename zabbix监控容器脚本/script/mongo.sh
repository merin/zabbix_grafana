#!/bin/bash
case $# in
  1)	
    output=$(sudo docker exec "$1" mongo --eval "db.serverStatus()" | sed -n '5,$p')
    ;;
  2)
    output=$(sudo docker exec "$1" mongo --eval "db.serverStatus().$2" |sed -n '5,$p')
    ;;
  3)
    output=$(sudo docker exec $1 mongo --eval "db.serverStatus().$2.$3" |sed -n '5,$p')
    #;;
esac
  
#check if the output contains "NumberLong"
if [[ "$output" =~ "NumberLong"   ]];then
  echo $output|sed -n 's/NumberLong(//p'|sed -n 's/)//p'
else
  echo $output
fi
